#include "Map.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

Map::Map()
{
    //ctor
}

Map::~Map()
{
    //dtorille
}
void Map::setBateauTouche(int nbBateau, int x, int y)
{
    bool existe = false;
    if(m_tabBateau[nbBateau].getTouche()==1){
        for(int i=0; i<m_tabBateau[nbBateau].getTouche_x().size(); i++)
    {
        if (x==m_tabBateau[nbBateau].getTouche_x()[i])
        {
            if(y==m_tabBateau[nbBateau].getTouche_y()[i])existe=true;
        }
    }
    }
    m_tabBateau[nbBateau].setTouche(true);
    // void setTouche(bool touche);
    if(!existe)m_tabBateau[nbBateau].setTouchePos(x, y);



}
void Map::ajoutGrille()
{

    for(int i=0; i<32; i++)
    {
        for(int j=0; j<47; j++)
        {
            if(getGrille(i,j)!=' ')
                setMatrice(i,j, getGrille(i,j));
        }
    }
}
void Map::suppressionBateau()
{
    bool existe=false;
    for(int i=0; i<m_tabBateau.size(); i++)
    {
        if(m_tabBateau[i].getPos_x().size()==m_tabBateau[i].getTouche_x().size())m_tabBateau.erase(m_tabBateau.begin()+i);
    }

}





vector <int> Map::getPosXBateau() const
{
    return m_PosXBateau;
}
vector <int> Map::getPosYBateau() const
{
    return m_PosYBateau;
}



char Map::getGrille(int i, int j) const
{

    return m_Grille[i][j];
}


char Map::getElemMatrice(int i, int j) const
{
    return m_Matrice[i][j];
}

Bateau Map::getBateau(int i) const
{
    return m_tabBateau[i];
}
vector<Bateau> Map::getBateau2() const
{
    return m_tabBateau;
}

int Map::getnbBateau()const
{
    return m_nbBateau;
}

void Map::setMatrice(int i, int j, char elem)
{
    m_Matrice[i][j]=elem;
}

void Map::addBateau()
{

    //srand(time(NULL));

    positionBateau(7); //ajout du cuirass�

    for(int i=0; i<2; i++)
    {
        positionBateau(5); //ajout des croiseurs
    }

    for(int i=0; i<3; i++)
    {
        positionBateau(3); //ajout des destroyers
    }

    for(int i=0; i<4; i++)
    {
        positionBateau(1);  //ajout des sous marins
    }


}
void Map::positionBateau(int type)
{
    bool posTrouve = true;
    int x=0;
    int y=0;
    int h=0;
    Bateau bat;
    while(posTrouve)
    {
        posTrouve = false;
        h = rand()%2;
        //cout<<" h:"<<h;
        x = rand()%15;
        if(h==0&&x+type>14)posTrouve=1;
        //cout<<" x:"<<x;
        y = rand()%15;
        if(h==1&&y+type>14)posTrouve=1;
        posTrouve = collision(x, y, h, type);
        if(h==0)if(x+type>14)posTrouve=1;
        if(h==1)if(y+type>14)posTrouve=1;


        if(!posTrouve)
        {
            for(int i=0; i<type; i++)
            {
                if(h==0)
                {
                    //cout<<x+type<<" ";
                    bat.setPos_x(x+i);
                    bat.setPos_y(y);
                    m_PosXBateau.push_back(x+i);
                    m_PosYBateau.push_back(y);

                }
                if(h==1)
                {
                    //cout<<y+type<<" ";
                    bat.setPos_x(x);
                    bat.setPos_y(y+i);
                    m_PosXBateau.push_back(x);
                    m_PosYBateau.push_back(y+i);
                }
            }
        }


    }

    bat.setHorizontal(h);
    bat.setVie(10);
    bat.setTouche(0);
    bat.setType(type);
    bat.setChoisi(0);
    m_tabBateau.push_back(bat);
    //cout<<" x"<<m_tabBateau[0].getPos_x2(0);


}

void Map::setNbBateau(int nb)
{
    m_nbBateau = 1;

}



void Map::selectionBateau(int nb)
{
    for(int i=0; i<m_tabBateau.size(); i++)
    {
        m_tabBateau[i].setChoisi(0);
    }
    m_tabBateau[nb].setChoisi(1);
}
int Map::tournerBateau(char lettre, int nb)
{

        bool existe = false;
    if(m_tabBateau[nb].getHorizontal()==1)
    {
        for(int k = 0; k<m_tabBateau[nb].getType(); k++)
        {
            for(int k = 0; k<m_tabBateau[nb].getType(); k++)
                for(int i=0; i<m_tabBateau.size(); i++)
            {int milieu = m_tabBateau[nb].getType()/2;
                for(int j=0; j<m_tabBateau[i].getType(); j++)
                {
                     if(k!=(int)m_tabBateau[nb].getType()/2)
                    {
                    if(m_tabBateau[nb].getPos_x2(milieu)-m_tabBateau[nb].getType()/2+k==m_tabBateau[i].getPos_x2(j)&&(!existe))
                                        {
                        existe = true;
                        if(existe)
                        {//cout<<"y="<<m_tabBateau[nb].getPos_y2(milieu)-m_tabBateau[nb].getType()/2+k;
                        //cout<<"x="<<m_tabBateau[nb].getPos_x2(milieu);
                            if(m_tabBateau[nb].getPos_y2(milieu)==m_tabBateau[i].getPos_y2(j)) existe = true;
                            else
                            {
                                existe = false;
                            }
                            //if(existe)cout<<"ici";
                        }
                    }
                    }

                }

            }


        }
    }



    if(m_tabBateau[nb].getHorizontal()==0)
    {
       for(int k = 0; k<m_tabBateau[nb].getType(); k++)
        {
            for(int k = 0; k<m_tabBateau[nb].getType(); k++)
                for(int i=0; i<m_tabBateau.size(); i++)
            { int milieu = m_tabBateau[nb].getType()/2;
                for(int j=0; j<m_tabBateau[i].getType(); j++)
                {
                    if(k!=(int)m_tabBateau[nb].getType()/2)
                    {


                    if(m_tabBateau[nb].getPos_y2(milieu)-m_tabBateau[nb].getType()/2+k==m_tabBateau[i].getPos_y2(j)&&(!existe))
                    {
                        existe = true;
                        if(existe)
                        {//cout<<"y="<<m_tabBateau[nb].getPos_y2(milieu)-m_tabBateau[nb].getType()/2+k;
                        //cout<<"x="<<m_tabBateau[nb].getPos_x2(milieu);

                       // cout<<
                            if(m_tabBateau[nb].getPos_x2(milieu)==m_tabBateau[i].getPos_x2(j))
                            {
                                existe = true;
                                //cout<<" y1 = "<<m_tabBateau[nb].getPos_y2(milieu)-m_tabBateau[nb].getType()/2+k;
                                //cout<<" x1 = "<<m_tabBateau[nb].getPos_x2(milieu);

                                //cout<<" y2 = "<<m_tabBateau[i].getPos_y2(j);
                                //cout<<"x2 = "<<m_tabBateau[i].getPos_x2(j);
                            }
                            else
                            {
                                existe = false;
                            }

                        }
                    }
                    }

                }

            }


        }

    }
int mouvement = 0;
    if((lettre =='w'||lettre=='W') && !existe)
    {
        mouvement = m_tabBateau[nb].rotation();
    }
    return mouvement;
    //cout<<mouvement;
}
int Map::MouvementBateau(char lettre, int nb)
{
    bool existe = false;
    int mouvement=0;

    if((lettre=='l'||lettre=='L')&& m_tabBateau[nb].getHorizontal()==0 && m_tabBateau[nb].getTouche()==0)
    {
        for(int i=0; i<m_tabBateau.size(); i++)
        {
            for(int j=0; j<m_tabBateau[i].getType(); j++)
            {
                if((m_tabBateau[nb].getPos_x2(m_tabBateau[nb].getType()-1)+1)==m_tabBateau[i].getPos_x2(j)&&(!existe))
                {
                    existe = true;
                    if(existe)
                    {
                        if(m_tabBateau[nb].getPos_y2(0)==m_tabBateau[i].getPos_y2(j)) existe = true;
                        else
                        {
                            existe = false;
                        }
                        //if(existe)cout<<"ici";
                    }
                }

            }

        }
        if(!existe)m_tabBateau[nb].mouvementX(1);
        if(!existe)mouvement=1;

        //cout<< m_tabBateau[0].getPos_x2(0);
    }
    if((lettre=='o'||lettre=='O')&& m_tabBateau[nb].getHorizontal()==0&& m_tabBateau[nb].getTouche()==0)
    {

        for(int i=0; i<m_tabBateau.size(); i++)
        {
            for(int j=0; j<m_tabBateau[i].getType(); j++)
            {
                if((m_tabBateau[nb].getPos_x2(0)-1)==m_tabBateau[i].getPos_x2(j)&&(!existe))
                {
                    existe = true;
                    if(existe)
                    {
                        if(m_tabBateau[nb].getPos_y2(0)==m_tabBateau[i].getPos_y2(j)) existe = true;
                        else
                        {
                            existe = false;
                        }
                        //if(existe)cout<<"ici";
                    }
                }

            }

        }

        m_tabBateau[nb].mouvementX(-1);


        if(!existe)mouvement=1;

        //cout<< m_tabBateau[0].getPos_x2(0);
    }


    if((lettre=='m'||lettre=='M')&& m_tabBateau[nb].getHorizontal()==1&& m_tabBateau[nb].getTouche()==0 )
    {
        for(int i=0; i<m_tabBateau.size(); i++)
        {
            for(int j=0; j<m_tabBateau[i].getType(); j++)
            {
                if((m_tabBateau[nb].getPos_y2(m_tabBateau[nb].getType()-1)+1)==m_tabBateau[i].getPos_y2(j)&&(!existe))
                {
                    existe = true;
                    if(existe)
                    {
                        if(m_tabBateau[nb].getPos_x2(0)==m_tabBateau[i].getPos_x2(j)) existe = true;
                        else
                        {
                            existe = false;
                        }
                        //if(existe)cout<<"ici";
                    }
                }

            }

        }

        if(!existe) m_tabBateau[nb].mouvementY(1);
        if(!existe)mouvement=1;
        //cout<< m_tabBateau[0].getPos_x2(0);
    }
    if((lettre=='k'||lettre=='K')&& m_tabBateau[nb].getHorizontal()==1&& m_tabBateau[nb].getTouche()==0)
    {
        for(int i=0; i<m_tabBateau.size(); i++)
        {
            for(int j=0; j<m_tabBateau[i].getType(); j++)
            {
                if((m_tabBateau[nb].getPos_y2(0)-1)==m_tabBateau[i].getPos_y2(j)&&(!existe))
                {
                    existe = true;
                    if(existe)
                    {
                        if(m_tabBateau[nb].getPos_x2(0)==m_tabBateau[i].getPos_x2(j)) existe = true;
                        else
                        {
                            existe = false;
                        }
                        //if(existe)cout<<"ici";
                    }
                }

            }

        }

        if(!existe)

            m_tabBateau[nb].mouvementY(-1);
        if(!existe)mouvement=1;

    }


    return mouvement;
}
bool Map::collision(int x, int y, int h, int type)
{

    bool existe = false;
    if(h==0)
    {
        for(int i=0; i<m_tabBateau.size(); i++)
        {
            for(int k = 0; k<type; k++)
            {
                for(int j=0; j<m_tabBateau[i].getType(); j++)
                {
                    if((x+k)==m_tabBateau[i].getPos_x2(j)&&(!existe))
                    {
                        existe = true;
                        if(existe)
                        {
                            if(y==m_tabBateau[i].getPos_y2(j)) existe = true;
                            else
                            {
                                existe = false;
                            }
                            //if(existe)cout<<"ici";
                        }
                    }

                }

            }


        }
    }



    if(h==1)
    {
        for(int i=0; i<m_tabBateau.size(); i++)
        {
            for(int k = 0; k<type; k++)
            {
                for(int j=0; j<m_tabBateau[i].getType(); j++)
                {
                    if((y+k)==m_tabBateau[i].getPos_y2(j)&&(!existe))
                    {
                        existe = true;
                        if(existe)
                        {
                            if(x==m_tabBateau[i].getPos_x2(j)) existe = true;
                            else
                            {
                                existe = false;
                            }
                            //if(existe)cout<<"ici";
                        }
                    }

                }

            }


        }

    }


    return existe;

}

void Map::ajoutBateauMatrice()
{

    for(int i=0; i<m_tabBateau.size(); i++)
    {
        for(int j=0; j<m_tabBateau[i].getType(); j++)
        {
            int x = (2*(m_tabBateau[i].getPos_x2(j)));
            x = x+2;
            int y =(3*(m_tabBateau[i].getPos_y2(j)));
            y=y+2;

            if(x>=32 || y>= 47)cout<<" x: "<<x<<" y: "<<y;
            if(m_tabBateau[i].getType() == 7)m_Grille[x][y]='c';
            if(m_tabBateau[i].getType() == 5)m_Grille[x][y]='r';
            if(m_tabBateau[i].getType() == 3)m_Grille[x][y]='d';
            if(m_tabBateau[i].getType() == 1)m_Grille[x][y]='s';
            if(m_tabBateau[i].getChoisi()) m_Grille[x][y]='m';
            if(m_tabBateau[i].getTouche())
            {
                for(int k=0; k<m_tabBateau[i].getTouche_x().size(); k++)
                {
                    if (m_tabBateau[i].getTouche_x()[k]==m_tabBateau[i].getPos_x2(j))
                    {
                        if(m_tabBateau[i].getTouche_y()[k]==m_tabBateau[i].getPos_y2(j))
                        {
                            m_Grille[x][y]=m_Grille[x][y]-32;
                        }
                    }

                }
                //if (m_tabBateau[i].getPos_x2(j)&&)
                //m_Grille[x][y]=m_Grille[x][y]-32;
            }
            m_Grille[x][y+1]=m_Grille[x][y];

            //m_Grille[x][y]='a';
            //cout<<getGrille(3,3);
            //m_Matrice[x][y]='a';
            //m_Matrice[10][21]='C';
            //cout<<m_tabBateau[i].getPos_x2(j);
            //cout<<" "<<m_tabBateau[i].getPos_x2(j);
            //cout<<" y : "<<y<<" x: "<<x;

        }
    }

}


void Map::affichage(Console* m_pConsole)
{
    m_pConsole->gotoLigCol(2,0);
    for(int i=0; i<32; i++)
    {
        m_pConsole->gotoLigCol(2+i,0);
        for(int j=0; j<47; j++)
        {
            if(getGrille(i, j)>=65&&getGrille(i, j)<=90)
            {
                m_pConsole->setColor(COLOR_RED);
            }
            else
            {
                if(getGrille(i, j)=='m')m_pConsole->setColor(COLOR_GREEN);
                else
                {
                    m_pConsole->setColor(COLOR_WHITE);
                }

            }

            cout<<getGrille(i, j);

        }
        cout<<endl;
    }
//int x = m_tabBateau[4].getPos_x()[0];


}


void Map::setGrille()
{
    int lettre = 0;
    int chiffre = 0;
    int cmp=0;
    bool booleen =0;
    int cmp2 = 0;
    int cmp3 = 0;
    for(int i=0; i<32; i++)
    {
        for(int j=0; j<47; j++)
        {

            m_Grille[i][j]=' ';
        }
    }
    for(int i=0; i<32; i++)
    {
        for(int j=0; j<47; j++)
        {
            if(j==0)
            {
                cmp2 = 1;
                cmp3=0;
            }
            cmp ++;
            cmp2++;
            cmp3++;
            if(i>0&&i<33)
            {

                if  (j==0&&(i%2)==0)
                {
                    m_Grille[i][j]='a'+lettre;
                    lettre++;
                }
            }
            if (i==0 && j>1 && j<46 && cmp == 3)
            {
                if(chiffre <= 9)
                {
                    m_Grille[i][j]=' ';
                    m_Grille[i][j+1]='0'+chiffre;

                }
                if(chiffre == 10)
                {
                    booleen = 1;
                    chiffre = 0;
                }
                if(booleen==1)
                {
                    m_Grille[i][j]='1';
                    m_Grille[i][j+1]='0'+chiffre;
                }
                cmp=0;
                chiffre++;
            }
            if (cmp2 == 3)
            {
                m_Grille[i][j]='|';
                cmp2=0;
            }
            if(cmp3==3&&(i%2!=0))
            {
                m_Grille[i][j]='_';
                m_Grille[i][j+1]='_';
                cmp3=0;
            }



        }
    }
}

bool Map::finPartie()
{
    bool fin =false;
    //if (m_tabBateau.size()==0)  fin=true;
    return fin;
}
void Map :: Menu()
{
    /*
       char choix;

        cout << "===============BATAILLE NAVALE===============\n" << endl;

       cout << "                       MENU                    \n" << endl;

       cout << "1. Commencer une partie" << endl;
       cout << "2. Charger une partie" << endl;
       cout << "3. R�gles du jeu" << endl;
       cout << "3. Quitter le jeu" << endl;
       cin << choix;

       switch (choix)
       {
       case 1 :
           /// commencer la partie
           break;

       case 2 :
           /// focntion charger partie
           break;
       case 3 :
           Regledujeu();
           /// fonction r�gles du jeu
           break;

       case 4 :
           /// quitter le jeu
           break;
       default:
           cout<< "Ce n'est pas un choix possible. Veuillez choisir une des options possible"
           break;
       }

       void Map:: Regledujeu()
       {
           cout<<"=============REGLE DU JEU=============\n";
           cout<<"Bienvenue sur le jeu Bataille Navale\nCe jeu se joue 2 contre 2.Le but du jeu est de coul� l'ensemble des bateaux adverses avant que sa flotte soit coul�.\n"
           cout<< "Chaque joueur poss�de 2 grille de 15 par 15. Sur la premi�re grille vous appercevrez vos 10 bateaux plac�s de facon al�atoire.\n La deuxi�me grille permet d'enregistrer vos diff�rents tirs au cours de la partie.\n\n"
           cout<<"Les Bateaux :\n\nVous disposez de 10 bateaux:\n1 cuirass� :\nTaille : 7 cases\nPuissance de tir : 9 cases.\n2 croiseurs :\nTaille :5 cases\nPuissance de tir :4cases\n3 desstoyers :\nTaille : 3cases\nPuissance de tir :1case\n Option: 1 fus�e eclairante affichant un carre de 4*4 cases\n4 sous_marin :\nTaille :1 case\nPuissance de tir :1case\nPropr�t� : Ne peut que couler des sous-marins adverses, pas de bteaux.\n\n"
           cout<<"Les bateaux ont la possiblit� de se d�placer horizontalement ou verticalement pendant la partie. Une fois qu'un bateau est touch� il ne peut plus se deplacer. Pour couler un bateau, il faut toucher la totalit� de ses cases.\n"
           cout<<"Actions :\n A chaque tour le joueur peut r�aliser une des 3 actions possibles:\nDeplacer son bateau s'il n'est pas affaibli d'une case horizontalement ou verticalement\nDeplacer son bateau s'il n'est pas affaibli de 90 degres\nTirer avec un bateau\n";
           cout<<"Le joueur pourra avec la touche p choisir le bateau avec lequel il souhaite realiser une action. Le bateau selectionn� sera color�. Pour tirer il faut selectionner une case de la grille puis appuyer sur entree. Le tir s'affiche alors sur la grille de tir. Si un bateau est touch� la grille de tir affichera une croix de couleur marquant la position de l'endroit ou se trouve un baeau\n\n"
           cout<<"Bonne partie. Quel le meilleur strat�ge GAGNE !!!!!!!!";
       }

    */
}

