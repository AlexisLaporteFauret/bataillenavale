#include "Bateau.h"
#include <iostream>
#include <string>
#include <vector>

Bateau::Bateau()
{

}
Bateau::Bateau(vector<int> pos_x,vector<int> pos_y,int type,int vie,bool horizontal)
{
    vector<int>::iterator it;
    int i=0;
    for(it=pos_x.begin(); it!=pos_x.end(); it++, i++)
    {
        m_pos_x.push_back(pos_x[i]);
    }
    for(it=pos_y.begin(); it!=pos_y.end(); it++, i++)
    {
        m_pos_y.push_back(pos_y[i]);
    }
    m_type = type;
    m_vie = vie;
    m_horizontal = horizontal;
}

void Bateau::setChoisi(bool choisi)
{
    m_choisi = choisi;
}
void Bateau::setPos_x(int pos_x)
{
    m_pos_x.push_back(pos_x);
}
vector<int> Bateau::getTouche_x()
{
    return m_touche_x;
}

vector<int> Bateau::getTouche_y()
{
    return m_touche_y;
}

void Bateau::setPos_y(int pos_y)
{
    m_pos_y.push_back(pos_y);
}

void Bateau::setType(int type)
{
    m_type = type;
}
void Bateau::setVie(int vie)
{
    m_vie = vie;
}

void Bateau::setHorizontal(bool horizontal)
{
    m_horizontal = horizontal;
}

void Bateau::setTouche(bool touche)
{
    if(m_touche == 0)
    {
        m_touche = touche;
    }
}
void Bateau::setTouchePos(int x, int y)
{
    m_touche_x.push_back(x);
    m_touche_y.push_back(y);
}
bool Bateau::getChoisi() const
{
    return m_choisi;
}

vector<int> Bateau::getPos_x() const
{
    return m_pos_x;
}

vector<int> Bateau::getPos_y() const
{
    return m_pos_y;
}
int Bateau::getPos_x2(int i) const
{
    return m_pos_x[i];
}
int Bateau::getPos_y2(int i) const
{
    return m_pos_y[i];
}

int Bateau::getType() const
{
    return m_type;
}

int Bateau::getVie() const
{
    return m_vie;
}

bool Bateau::getTouche() const
{
    return m_touche;
}

bool Bateau::getHorizontal() const
{
    return m_horizontal;
}
void Bateau::mouvementX(int x)
{
    if(x==1 && m_pos_x[m_type-1]+x<=14 && m_horizontal==0)
    {
        for(int i=0; i<m_type; i++)
        {
            m_pos_x[i]= m_pos_x[i]+x;
        }
    }
    if(x==-1 && m_pos_x[0]+x>=0 &&m_horizontal==0)
    {
        for(int i=0; i<m_type; i++)
        {
            m_pos_x[i]= m_pos_x[i]+x;
        }
    }

}
int Bateau::rotation()
{
    int mouvement = 0;
    if(m_horizontal==0)
    {
       // m_horizontal=1;
        int milieu = m_type/2;
        if(m_pos_y[milieu]+m_type/2<=14&&m_pos_y[milieu]-m_type/2>=0)
        {
            m_horizontal=1;
            int x= m_pos_x[milieu];

            for(int i=0; i<m_type; i++)
            {
                //int x= m_pos_x[milieu];
                m_pos_y[i] = m_pos_y[milieu]-(m_type/2)+i;
                m_pos_x[i] = x;
                mouvement = 1;
            }


        }
    }

    else
    {
        //m_horizontal=0;
        int milieu = m_type/2;
        if(m_pos_x[milieu]+m_type/2<=14&&m_pos_x[milieu]-m_type/2>=0)
        { m_horizontal=0;
            int y= m_pos_y[milieu];
            //int x= m_pos_x[0];
            for(int i=0; i<m_type; i++)
            {
                //int x= m_pos_x[i];
                m_pos_x[i] = m_pos_x[milieu]-(m_type/2)+i;
                m_pos_y[i] = y;
                mouvement = 1;
            }


        }
    }
    //cout<<mouvement;
    return mouvement;

}
void Bateau::mouvementY(int y)
{
    {
        if(y==1 && m_pos_y[m_type-1]+y<=14 && m_horizontal==1)
        {
            for(int i=0; i<m_type; i++)
            {
                m_pos_y[i]= m_pos_y[i]+y;
            }
        }
        if(y==-1 && m_pos_y[0]+y>=0 &&m_horizontal==1)
        {
            for(int i=0; i<m_type; i++)
            {
                m_pos_y[i]= m_pos_y[i]+y;
            }
        }

    }
}

Bateau::~Bateau()
{
    //dtor
}



