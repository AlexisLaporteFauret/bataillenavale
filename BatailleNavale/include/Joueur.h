#ifndef JOUEUR_H
#define JOUEUR_H
#include <iostream>
#include "Map.h"
#include <cstdlib>
#include <ctime>
#include "Bateau.h"
#include <time.h>
#include "windows.h"
#include "console.h"
#include "Tir.h"
class Joueur
{
    public:
        Joueur();
        virtual ~Joueur();
        bool tour(Console* m_pConsole, int choix, int nbJoueur,  bool debut);
        void setTir(int x, int y);
        void setTouche(int x, int y ,int nbBateau);
        void init();

        Map getMap()const;
        Tir getTir()const;


    protected:
    private:
        Map m_map;
        Tir m_tir;
        //Console* m_pConsole;


};

#endif // JOUEUR_H
