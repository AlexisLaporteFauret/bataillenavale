#ifndef MAP_H
#define MAP_H
#include "Bateau.h"
#include <iostream>
#include <vector>
#include"Console.h"
using namespace std;


class Map
{
    public:
        Map();
        virtual ~Map();
        void affichage(Console* m_pConsole);
        void ajoutGrille();
        void remplissageGrille();

        void positionBateau( int type);
        void addBateau();
        void ajoutBateauMatrice();
        void Menu();
        void Regledujeu();

        char getGrille(int i, int j) const;
        char getElemMatrice(int i, int j) const;
        Bateau getBateau(int i) const;
        vector<Bateau> getBateau2() const;
        vector <int> getPosXBateau() const;
        vector <int> getPosYBateau() const;
        int getnbBateau()const;

        int tournerBateau(char lettre, int nb);
        bool collision(int x, int y, int h, int type);

        void affichageGrille();
        int MouvementBateau(char lettre, int nb);
        void setMatrice(int i, int j, char elem);
        void addBateau(Bateau bat);
        void setNbBateau(int nb);
        void setGrille();
        void selectionBateau(int nb);
        void setBateauTouche(int nbBateau, int x, int y);
        void init();
        void suppressionBateau();
        bool finPartie();



    protected:
    private:

        char m_Matrice[17][47];
        vector<Bateau> m_tabBateau;
        int m_nbBateau;
        char m_Grille[32][47];
        vector<int> m_PosXBateau;
        vector<int> m_PosYBateau;


};

#endif // MAP_H
