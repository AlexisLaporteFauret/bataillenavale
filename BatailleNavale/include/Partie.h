#ifndef PARTIE_H
#define PARTIE_H
#include "Joueur.h"
#include <iostream>
#include "Map.h"
#include <cstdlib>
#include <ctime>
#include "Bateau.h"
#include <time.h>
#include "windows.h"
#include "console.h"
#include "Tir.h"


class Partie
{
    public:
        Partie();
        virtual ~Partie();
        int jeu();
        int Choix(int nbJoueur);
        void Collision(int nbJoueur);

    protected:
    private:
        Joueur m_joueur1;
        Joueur m_joueur2;
        Console* m_pConsole;

};

#endif // PARTIE_H
