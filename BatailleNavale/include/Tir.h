//diff a/BatailleNavale/include/Tir.h b/BatailleNavale/include/Tir.h	(rejected hunks)
//@@ -0,0 +1,22 @@
#ifndef TIR_H
#define TIR_H
#include "Bateau.h"
#include<vector>
#include"Console.h"

using namespace std;


class Tir
{
    public:
        Tir();
        virtual ~Tir();
        void boom(int m_type);
        void setPos_x(int posx);
        void setPos_y(int posy);
        void setPos(int type);
        void setMatTir(char grille, int i,int j);
        void setTouche_x(int x);
        void setTouche_y(int y);

        void affichage(Console * m_pConsole);
        void viser(char key, int type);
        int getPos_x(int i) const;
        int getPos_y(int i) const;
        void initMatTir();
        void ajoutViseur();
        vector<int> getPos_x2()const;
        vector<int> getPos_y2()const;
        vector<int> getTouche_x()const;
        vector<int> getTouche_y()const;



    protected:
    private:
        vector<int> m_pos_x;
        vector<int> m_pos_y;
        char m_matTir[32][47];
        vector<char> cible;
        vector<int> m_touche_x;
        vector<int> m_touche_y;
};

#endif // TIR_H
